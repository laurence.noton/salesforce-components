# SFDX App

A custom picklist to replace the out of the box one provided by Salesforce.

### Challenges of OOTB Picklist

The challange with the OOTB version is that it:

- it is always Required
- it defaults to a selected value (thus always validating)
- it doesnt let you configure a manual choice 

### Features
This version attempts to address these challenges:

- Specify and object-field picklist to be the datasource of the picklist
- will always insert a "Please select..." value at the top and preselect it.
- validation can be turned on or off
- support for manual entry (see below)

#### Manual Value
The component can be configured to request a manually typed.  This field will be prompted for if a specific selection from the drop down is chosen. The option is defined as design time, and during runtime if the option is not already in the picklist it will automatically be added.

- Enable feature by setting "Manual override" property to True.
- Define which drop down value triggers manual request "Manual option".

### Data

manual - value from the drop down
manualValue - value from the text box (only appears if Manual is turned on)


## Issues
