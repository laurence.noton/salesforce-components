({
	doInit : function(component, event, helper) {
        helper.callAction( component, 'c.getPicklistOptions', {
            	'objectName' : component.get('v.objectName'),
            	'fieldName'  : component.get('v.fieldName')
        	}, function(data) {
                var manualOptionName = component.get('v.manualOptionName');

                const res = data.some(option => option.value == manualOptionName);
                if (!res) {
                    data.push({'value': manualOptionName, 'label' : manualOptionName});
                }

                component.set('v.options', data);
        });
        component.set('v.validate', function() {
            var value = component.get('v.value');
            var manualValue = component.get('v.manualValue');
            var isRequired = component.get('v.isRequired');
            var manualOptionName = component.get('v.manualOptionName');
            var isManualOtherAvailable = component.get('v.isManualOtherAvailable');

            if (isRequired && (value == "")) {
                return { isValid: false, errorMessage: 'An option must be selected.' };
            } else if (isRequired && isManualOtherAvailable && (value == manualOptionName && (manualValue == "" || manualValue === undefined))) {
                return { isValid: false, errorMessage: 'Please type in a response.' };
            }else  {
                return { isValid: true };
            }
        });
    },

    onChangeOption: function (component, event, helper) {
        component.set('v.manualValue', '');
    }
})